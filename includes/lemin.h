/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 23:34:22 by mdelory           #+#    #+#             */
/*   Updated: 2019/09/23 19:56:45 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# define LM_START 		1
# define LM_END 		2

# define LM_FORWARD		0b01
# define LM_BACKWARD	0b10

# include "libft.h"

typedef struct		s_room
{
	char			*name;
	int				x;
	int				y;
	int				flag;
}					t_room;

typedef struct		s_lemin
{
	int				nb_ant;
	int				nb_room;
	int				*visited;
	int				*parent;
	int				**graph;
	int				**flow;
	struct s_batch	*batch;
	t_vector		v_room;
	t_vector		v_data;
}					t_lemin;

typedef struct		s_batch
{
	int				time;
	int				total_size;
	int				*path_size;
	int				*path_limit;
	t_room			**path_lst;
	int				nb_path;
}					t_batch;

typedef struct		s_ant
{
	int				id;
	int				arrived;
	int				pos;
	t_room			*path;
}					t_ant;

int					display_arg(t_lemin *lem, char **str);
int					init_graph(t_lemin *lem, int r1, int r2);

int					lem_parse(t_lemin *lem);
int					lem_solve(t_lemin *lem);

void				do_batch(t_lemin *lem, t_batch *b, int *end, int i);
void				apply_flow(t_lemin *lem, int end);

t_room				*create_room(char *str, int x, int y, int flag);
t_room				*get_room(t_lemin *lem, char *str);
t_room				*get_room_by_id(t_lemin *lem, int i);
int					get_room_id(t_lemin *lem, char *str);
int					room_path(t_lemin *lem, int room);

t_batch				*get_batch(t_lemin *lem, int nb_path);
int					calc_batch(t_lemin *lem, t_batch *b);
void				del_batch(t_batch *b);

int					check_one_turn(t_lemin *l);
int					lem_dispatch(t_lemin *lem);
#endif
