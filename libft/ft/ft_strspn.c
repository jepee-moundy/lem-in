/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strspn.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 15:34:45 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:51 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_strspn(const char *s, const char *charset)
{
	const char	*ptr;

	ptr = s;
	while (*ptr && ft_strchr(charset, *ptr))
		ptr++;
	return ((size_t)(ptr - s));
}
