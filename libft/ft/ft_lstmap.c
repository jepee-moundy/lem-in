/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 20:29:43 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:35 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list		*new;
	t_list		*tmp;

	if (!lst)
		return (NULL);
	if (lst->next)
		new = ft_lstmap(lst->next, (*f));
	tmp = (*f)(lst);
	ft_lstadd(&new, tmp);
	return (new);
}
