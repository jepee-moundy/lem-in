/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 19:42:33 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:36 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list		*cur;
	t_list		*tmp;

	cur = *alst;
	while (cur)
	{
		tmp = cur->next;
		ft_lstdelone(&cur, (*del));
		cur = tmp;
	}
	*alst = NULL;
}
