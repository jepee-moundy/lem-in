/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 21:32:00 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:04:27 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strdup(const char *s1)
{
	size_t		len;
	char		*s2;

	len = ft_strlen(s1);
	s2 = (char *)malloc(len * sizeof(char) + 1);
	if (s2 != NULL)
		s2 = ft_strcpy(s2, s1);
	return (s2);
}
