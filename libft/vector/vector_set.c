/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_set.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:21:49 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:06 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

int		vector_set(t_vector *v, unsigned int index, void *data)
{
	if (!v || index > v->count || !v->data)
		return (1);
	v->data[index] = data;
	return (0);
}
