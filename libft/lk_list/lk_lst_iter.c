/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lk_lst_iter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 20:39:10 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:58 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				lk_lst_iter(t_lklist *lst, void (*f)(t_lknode *el))
{
	t_lknode			*el;

	if (!lst || !f)
		return ;
	el = lst->head;
	while (el)
	{
		(*f)(el);
		el = el->next;
	}
}
