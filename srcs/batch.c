/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   batch.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 15:11:45 by mdelory           #+#    #+#             */
/*   Updated: 2019/10/03 16:54:15 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static t_batch	*create_batch(int nb_path)
{
	t_batch		*b;

	if (!(b = (t_batch *)malloc(sizeof(t_batch))))
		return (NULL);
	b->time = 0;
	b->nb_path = 0;
	b->total_size = 0;
	if (!(b->path_limit = (int *)malloc(sizeof(int) * nb_path)))
		return (NULL);
	if (!(b->path_size = (int *)malloc(sizeof(int) * nb_path)))
		return (NULL);
	if (!(b->path_lst = (t_room **)malloc(sizeof(t_room *) * nb_path)))
		return (NULL);
	return (b);
}

static t_room	*extract_path(t_lemin *lem, t_batch *b, int room, int i)
{
	t_room		*path;
	int			n;

	n = 0;
	while (n < lem->nb_room && lem->flow[room][n] != 1)
		n++;
	if (get_room_by_id(lem, room)->flag == LM_END)
	{
		path = (t_room *)malloc(sizeof(t_room) * (i + 1));
		b->path_size[b->nb_path] = i + 1;
		b->total_size += i;
	}
	else
		path = extract_path(lem, b, n, i + 1);
	path[i] = *(get_room_by_id(lem, room));
	return (path);
}

void			del_batch(t_batch *b)
{
	int			i;

	i = -1;
	while (++i < b->nb_path)
		free(b->path_lst[i]);
	free(b->path_size);
	free(b->path_limit);
	free(b->path_lst);
	free(b);
}

t_batch			*get_batch(t_lemin *lem, int nb_path)
{
	t_batch		*b;
	int			i;
	int			j;

	i = 0;
	b = create_batch(nb_path);
	while (i < lem->nb_room && b)
	{
		if (get_room_by_id(lem, i)->flag == LM_START)
		{
			j = 0;
			while (j < lem->nb_room)
			{
				if (lem->flow[i][j] == 1)
				{
					b->path_lst[b->nb_path] = extract_path(lem, b, j, 1);
					b->nb_path++;
				}
				j++;
			}
		}
		i++;
	}
	return (b);
}

int				calc_batch(t_lemin *lem, t_batch *b)
{
	int			i;
	int			m;

	i = 0;
	m = (lem->nb_ant + b->total_size) % b->nb_path;
	b->time = ((lem->nb_ant + b->total_size) / b->nb_path) + !!m;
	while (i < b->nb_path)
	{
		b->path_limit[i] = b->time - b->path_size[i] + 1;
		i++;
	}
	return (b->time);
}
